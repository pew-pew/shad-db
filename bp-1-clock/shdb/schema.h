#pragma once

#include <string>
#include <vector>

namespace shdb {

enum class Type {
    boolean,
    uint64,
    varchar,
};

struct ColumnSchema
{
    std::string name;
    Type type;
    int length;
};

using Schema = std::vector<ColumnSchema>;

}    // namespace shdb
